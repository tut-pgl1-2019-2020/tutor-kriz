﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tut_0
{
    class School
    {
        private string name;
        private List<Student> students;

        public School(string _name)
        {
            this.name = _name;
            this.students = new List<Student>();
        }

        public void AddStudent(Student student)
        {
            var studentFullName = student.FullName;
            foreach(var st in this.students)
            {
                if(st.FullName == studentFullName)
                {
                    throw new Exception("Student již existuje.");
                }
            }
            this.students.Add(student);
        }
        public string GetStudents()
        {
            string vypis = "";
            foreach (var st in this.students)
            {
                vypis += $"Jméno: {st.FullName}\nVěk: {st.Age}\n\n";
            }
            return vypis;
        }
    }
}
