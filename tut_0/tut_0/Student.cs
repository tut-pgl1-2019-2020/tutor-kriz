﻿using System;
using System.Collections.Generic;

namespace tut_0
{
    class Student
    {
        private string stagId;
        private string firstName;
        private string lastName;
        private DateTime birthDate;
        private bool active; //jestli studuje bo ne

        public Student(string _first, string _last, DateTime bd, bool active)
        {
            this.stagId = "st"+ _first + _last + bd.Year;
            this.firstName = _first;
            this.lastName = _last;
            this.birthDate = bd;
            this.active = active;
        }

        public int Age
        {
            get
            {
                var navrat = DateTime.Now.Year - this.birthDate.Year;
                return navrat;
            }
        }
        public string FullName
        {
            get
            {
                var navrat = $"{this.firstName} {this.lastName}";
                return navrat;
            }
        }
        
        public void Vyhod()
        {
            if (this.active)
            {
                this.active = !this.active;
            }
            else
            {
                throw new Exception("Tož nestuduje");
            }
        }
    }
}
